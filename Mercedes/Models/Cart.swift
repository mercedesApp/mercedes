//
//  Cart.swift
//  Mercedes
//
//  Created by El houssaine El gamouz on 28/05/2018.
//  Copyright © 2018 majjane. All rights reserved.
//

import SwiftyJSON

class Cart {
    
    var id: Int?
    var type: String?
    var name: String?
  
    
    init(fromJSON json: JSON) {
        self.id = json["id"].intValue
        self.type = json["cc_type"].stringValue
        self.name = json["name"].stringValue
    }
}
