//
//  ProductApiClient.swift
//  Mercedes
//
//  Created by El houssaine El gamouz on 23/05/2018.
//  Copyright © 2018 majjane. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ProductApiClient: BaseApiClient {


    static func products(_ data: URLRequestParams, success: @escaping ([Product]) -> Void, failure: @escaping (ApiError) -> Void ) {
        
         Alamofire.request(Router.products(data: data))
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    let json = JSON(data: response.data!)
                    var products = [Product]()
                    for productJSON in json["_embedded"]["items"].arrayValue{
                        let product = Product(fromJSON: productJSON)
                        products.append(product)
                    }
                    
                    success(products)
                case .failure(_):
                    let apiError = ApiError(response: response)
                    failure(apiError)
                }
        }
    }

    static func product(_ code: String, success: @escaping (Product) -> Void, failure: @escaping (ApiError) -> Void) {
        Alamofire.request(Router.product(code: code))
            .validate()
            .responseJSON{ response in
                switch response.result {
                case .success:
                    let json = JSON(data: response.data!)
                    let product = Product(fromJSON: json)
                    for reviewJSON in json["reviews"].arrayValue {
                        let review = ProductReview(fromJSON: reviewJSON)
                        product.reviews.append(review)
                    }
                    success(product)
                case .failure(_):
                    let apiError = ApiError(response: response)
                    failure(apiError)
                }
        }
    }
}
