//
//  ShopViewController.swift
//  Mercedes
//
//  Created by El houssaine El gamouz on 04/05/2018.
//  Copyright © 2018 majjane. All rights reserved.
//

import UIKit
import AACarousel


class ShopViewController: UIViewController {
    
    var AccessoriesViewController: UIViewController!
    var CollectionViewController: UIViewController!
    var ProductsViewController: UIViewController!
    var SpecialsViewController: UIViewController!
    var CartViewController: UIViewController!
    var viewControllers: [UIViewController]!
    var titleArray = [String]()
    var isLoading = true
    var products = [Product]()
    var indexPath:IndexPath?
    
    @IBOutlet weak var bestsalescollectionView: UICollectionView!
    @IBOutlet weak var newsAacarousel: AACarousel!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet var buttons: [UITabBarItem]!
    @IBOutlet weak var tabBar: UITabBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Instantiate  and  particular ViewController's Storyboard ID
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        AccessoriesViewController = storyboard.instantiateViewController(withIdentifier: "AccessoriesController")
        CollectionViewController = storyboard.instantiateViewController(withIdentifier: "CollectionController")
        ProductsViewController = storyboard.instantiateViewController(withIdentifier: "ProductsController")
        SpecialsViewController = storyboard.instantiateViewController(withIdentifier: "SpecialsController")
        CartViewController = storyboard.instantiateViewController(withIdentifier: "CartController")
        
        //Filling viewControllers array
        viewControllers = [AccessoriesViewController,CollectionViewController, ProductsViewController, SpecialsViewController, CartViewController]
        //Filling News Slider
        let pathArray = ["http://www.gettyimages.ca/gi-resources/images/Embed/new/embed2.jpg",
                         "https://ak.picdn.net/assets/cms/97e1dd3f8a3ecb81356fe754a1a113f31b6dbfd4-stock-photo-photo-of-a-common-kingfisher-alcedo-atthis-adult-male-perched-on-a-lichen-covered-branch-107647640.jpg",
                         "https://imgct2.aeplcdn.com/img/800x600/car-data/big/honda-amaze-image-12749.png",
                         "http://www.conversion-uplift.co.uk/wp-content/uploads/2016/09/Lamborghini-Huracan-Image-672x372.jpg",
                         "very-large-flamingo"]
        titleArray = ["picture 1","picture 2","picture 3","picture 4","picture 5"]
        newsAacarousel.delegate = self
        newsAacarousel.setCarouselData(paths: pathArray,  describedTitle: titleArray, isAutoScroll: true, timer: 5.0, defaultImage: "defaultImage")
        //optional methods
        newsAacarousel.setCarouselOpaque(layer: false, describedTitle: false, pageIndicator: false)
        newsAacarousel.setCarouselLayout(displayStyle: 0, pageIndicatorPositon: 5, pageIndicatorColor: nil, describedTitleColor: nil, layerColor: nil)
        
        // UITabBarDelegate
        self.tabBar.delegate = self
        
        //call
       fetchProducts()
        
    }
    
    // UITabBarDelegate
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        let vc = viewControllers[item.tag]
        vc.view.frame = contentView.bounds
        contentView.addSubview(vc.view)
    }
    
    override func willRotate(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
        // Display StatusBar in Mode landscape
        UIApplication.shared.setStatusBarHidden(false, with: .none)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        //Show ProductDetailViewController
        if segue.identifier == "productDetail" {
            let destinationNavigationController = segue.destination as! UINavigationController
            let productDetailViewController = destinationNavigationController.topViewController as! ProductDetailViewController
            if let cell = sender as? UICollectionViewCell {
                if let indexPath = bestsalescollectionView.indexPath(for: cell) {
                    productDetailViewController.productCode = products[indexPath.row].code!
                }
            }
        }
    }
    
    //require method
    func downloadImages(_ url: String, _ index: Int) {
        //here is download images area
        let imageView = UIImageView()
    }
    
}

extension ShopViewController:  UITabBarDelegate,UITabBarControllerDelegate {}
private extension ShopViewController {
    
    func fetchProducts() {
        
        var data = URLRequestParams()
        ProductApiClient.products(data, success: { products in
            self.products += products
            //self.isLoading = false
            self.bestsalescollectionView.reloadData()
            let productGridFlowLayout = ProductGridFlowLayout()
            self.bestsalescollectionView.collectionViewLayout.invalidateLayout()
            self.bestsalescollectionView.setCollectionViewLayout(productGridFlowLayout, animated: false)
        }, failure: { apiError in
            self.showApiErrorAlert(apiError)
        })
    }
}
extension ShopViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cell = collectionView.cellForItem(at: indexPath)!
        performSegue(withIdentifier: "productDetail", sender: cell)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let position = scrollView.bounds.origin.y
        let distanceFromBottom = scrollView.contentSize.height - scrollView.contentOffset.y - scrollView.frame.size.height
        
        if position < 0 {
            // navBarView.scrollToPrimaryNavBar()
        } else if distanceFromBottom < 0 {
            //navBarView.scrollToSecondaryNavBar()
        } else {
            // navBarView.scrollTo(position: position)
        }
    }
    
}

extension ShopViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        print(products.count)
        return products.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = bestsalescollectionView.dequeueReusableCell(withReuseIdentifier: "ProductGridCell", for: indexPath) as! ProductCell
        let product = products[indexPath.row]
        cell.configure(forProduct: product)
        return cell
    }
    
}

extension ShopViewController: AACarouselDelegate{
    func didSelectCarouselView(_ view: AACarousel, _ index: Int) {
        print(index)
    }
    
    func callBackFirstDisplayView(_ imageView: UIImageView, _ url: [String], _ index: Int) {
        /* imageView.kf.setImage(with: URL(string: url[index]), placeholder: UIImage.init(named: "defaultImage"), options: [.transition(.fade(1))], progressBlock: nil, completionHandler: nil)*/
        
    }
    
    func startAutoScroll() {
        //optional method
        newsAacarousel.startScrollImageView()
        
    }
    
    func stopAutoScroll() {
        //optional method
        newsAacarousel.stopScrollImageView()
    }
    
    
}
extension ShopViewController {
    
    func showAlert(_ title: String, message: String, handler: ((UIAlertAction) -> Void)?) {
        let ac = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default, handler: handler)
        ac.addAction(ok)
        present(ac, animated: true, completion: nil)
    }
    
    func showApiSuccessAlert(_ message: String) {
        showAlert("Yippiee!!!", message: message, handler: { _ in
            self.dismiss(animated: true, completion: nil)
        })
    }
    
    func showApiErrorAlert(_ apiError: ApiError) {
        showAlert("Whooops!!!", message: apiError.errorMessage(), handler: nil)
    }
    
    
}


