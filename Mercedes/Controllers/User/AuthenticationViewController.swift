//
//  AuthenticationViewController.swift
//  Mercedes
//
//  Created by El houssaine El gamouz on 23/05/2018.
//  Copyright © 2018 majjane. All rights reserved.
//

import UIKit

protocol AuthenticationViewDelegate: class {
    func switchToSignInView(_ controller: BaseViewController)
    func switchToSignUpView(_ controller: BaseViewController)
}

class AuthenticationViewController: BaseViewController {

    @IBOutlet weak var signInView: UIView!
    @IBOutlet weak var signUpView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "signIn" {
            if let controller = segue.destination as? SignInViewController {
                controller.delegate = self
            }
        } else if segue.identifier == "signUp" {
            if let controller = segue.destination as? SignUpViewController {
                controller.delegate = self
            }
        }
    }
    
    func leftPane() -> CGRect {
        return CGRect( x: -120, y: 64, width: view.bounds.width, height: view.bounds.height - self.navigationController!.navigationBar.frame.size.height )
    }
    
    func centerPane() -> CGRect {
        return CGRect( x: 0, y: 64, width: view.bounds.width, height: view.bounds.height - self.navigationController!.navigationBar.frame.size.height)
    }
    
    func rightPane() -> CGRect {
        return CGRect( x: view.bounds.width, y: 64, width: view.bounds.width, height: view.bounds.height - self.navigationController!.navigationBar.frame.size.height )
    }
 

}
extension AuthenticationViewController: AuthenticationViewDelegate {
    func switchToSignInView(_ controller: BaseViewController) {
        UIView.animate(withDuration: 0.4, delay: 0, options: UIViewAnimationOptions(), animations: {
            self.signInView.frame = self.centerPane()
        }, completion: nil)
        
        UIView.animate(withDuration: 0.4, delay: 0, options: UIViewAnimationOptions(), animations: {
            self.signUpView.frame = self.rightPane()
        }, completion:  nil)
    }
    
    func switchToSignUpView(_ controller: BaseViewController) {
        UIView.animate(withDuration: 0.4, delay: 0, options: UIViewAnimationOptions(), animations: {
            self.signUpView.frame = self.centerPane()
        }, completion: nil)
        
        UIView.animate(withDuration: 0.4, delay: 0, options: UIViewAnimationOptions(), animations: {
            self.signInView.frame = self.leftPane()
        }, completion: nil)
    }
}
