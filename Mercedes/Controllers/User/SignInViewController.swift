//
//  SignInViewController.swift
//  Mercedes
//
//  Created by El houssaine El gamouz on 23/05/2018.
//  Copyright © 2018 majjane. All rights reserved.
//

import UIKit
import Alamofire


class SignInViewController: BaseViewController {
 
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    
    @IBAction func login(_ sender: UIButton) {
        UserApiClient.login(requestData(), success: { user in
            User.currentUser = user
            self.delegate?.dismiss(animated: true, completion: nil)
        }, failure: { apiError in
            self.showApiErrorAlert(apiError)
        })
    }
    
    var delegate: AuthenticationViewController?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func requestData()-> URLRequestParams{
        var data = URLRequestParams()
        
        data["client_id"] = "52wpam5g2xkwoksg4oc4c8o80k08w0gcg84okcgs00s00cgcw0" as AnyObject?
        data["client_secret"] = "4jc726mn078k8ko8css8kk4kscg0sgws8k0o4koc00s80ks8ks" as AnyObject?
        data["grant_type"] = "password" as AnyObject?
        data["username"] = emailTextField.text! as AnyObject?
        data["password"] = passwordTextField.text! as AnyObject?
     
        return data
    }

    @IBAction func switchToSignUpView(_ sender: UIButton) {
     delegate?.switchToSignUpView(self)
    }

}
