//
//  CustomerAreaViewController.swift
//  Mercedes
//
//  Created by El houssaine El gamouz on 29/05/2018.
//  Copyright © 2018 majjane. All rights reserved.
//

import UIKit

class CustomerAreaViewController:  UIViewController {
    
    var AccountInfoViewController: UIViewController!
    var HistoricViewController: UIViewController!
    var LoyaltyPointsViewController: UIViewController!
    var FavoritesViewController: UIViewController!
    var viewControllers: [UIViewController]!
    
    
    // Mark: - Outlets
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var tabBar: UITabBar!
    
    // Mark: - Actions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Instantiate  and  particular ViewController's Storyboard ID
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        AccountInfoViewController = storyboard.instantiateViewController(withIdentifier: "AccountInfoViewController")
        HistoricViewController = storyboard.instantiateViewController(withIdentifier: "HistoricViewController")
        LoyaltyPointsViewController = storyboard.instantiateViewController(withIdentifier: "LoyaltyPointsViewController")
        FavoritesViewController = storyboard.instantiateViewController(withIdentifier: "FavoritesViewController")
        
        //Filling viewControllers array
        viewControllers = [AccountInfoViewController,HistoricViewController, LoyaltyPointsViewController, FavoritesViewController]
        
        //Load First ViewController
        selectViewController(itemtag:0)
        
        // UITabBarDelegates
        self.tabBar.delegate = self
        
    }
    
    // UITabBarDelegate
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        selectViewController(itemtag:item.tag)
    }
    
    //Select ViewController
    func selectViewController(itemtag:Int){
        let vc = viewControllers[itemtag]
        vc.view.frame = contentView.bounds
        contentView.addSubview(vc.view)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension CustomerAreaViewController : UITabBarDelegate{}








