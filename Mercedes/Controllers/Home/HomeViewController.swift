//
//  HomeViewController.swift
//  Mercedes
//
//  Created by El houssaine El gamouz on 29/05/2018.
//  Copyright © 2018 majjane. All rights reserved.
//

import UIKit

class HomeViewController: BaseViewController {

    var currentPageNo = 1
    let totalPages = 4
    let productsPerPage = 10
    
    
    
    /* @IBAction func gmailSignIn(_ sender: Any) {
     
     /* GmailClass.sharedInstance().loginWithGmail(viewController: self, successHandler: { (response) in
     
     
     }, failHandler: { (failResponse) in
     
     /* self.activity.isHidden = true
     self.btnGmailLogin.isHidden = false*/
     })*/
     
     
     FacebookClass.sharedInstance().loginWithFacebook(viewController: self, successHandler: { (response) in
     
     print("facebookfacebook")
     print(response)
     print("facebookfacebook")
     
     }, failHandler: { (failResponse) in
     print("failResponse")
     /* self.activity.isHidden = true
     self.btnFacebookLogin.isHidden = false*/
     })
     }*/
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //configure StatusBar
        configureStatusBar()
        //fetchProducts(currentPageNo)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func willRotate(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
        UIApplication.shared.setStatusBarHidden(false, with: .none)
    }
    
    
    func fetchProducts(_ pageNo: Int) {
        
        var data = URLRequestParams()
        data["page"] = currentPageNo as AnyObject?
        data["per_page"] = productsPerPage as AnyObject?
        
        ProductApiClient.products(data, success: { products in
            
        }, failure: { apiError in
            //self.showApiErrorAlert(apiError)
        })
    }
    
    
    
    func configureStatusBar() {
        UIApplication.shared.setStatusBarHidden(false, with: .none)
    }
    
}


