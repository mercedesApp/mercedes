//
//  ProductDetailViewController.swift
//  Mercedes
//
//  Created by El houssaine El gamouz on 07/05/2018.
//  Copyright © 2018 majjane. All rights reserved.
//

import UIKit

class ProductDetailViewController: UIViewController {
    
    var productCode:String?
    var product: Product?
    var selectedVariant: Variant?
    
    @IBAction func addToCart(_ sender: UIButton) {

        if !Order.hasCurrentOrder {
            CartApiClient.createCart(requestData(), success: { cart in
               // Cart.currentCart = cart
                self.addProductToCart()
            }, failure: { apiError in
                self.showApiErrorAlert(apiError)
            })
        } else {
            addProductToCart()
        }
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        fetchProduct()
    }
    
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func requestData()-> URLRequestParams{
        var data = URLRequestParams()
        
        data["customer"] = "shop@example.com" as AnyObject?
        data["channel"] = "US_WEB" as AnyObject?
        data["localeCode"] = "en_US" as AnyObject?
        
        return data
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

private extension ProductDetailViewController{
    func fetchProduct() {
        ProductApiClient.product(productCode!, success: { product in
            self.product = product
            self.fillInProductDetails()
        }, failure: { apiError in
            self.showApiErrorAlert(apiError)
        })
    }
    
    func addProductToCart() {
        if User.currentUser == nil {
           // alert(message: "Please sign in to add this product to your cart")
            return
        }
        
        if selectedVariant == nil {
           // alert(message: "Please select a variant to add to your cart")
            return
        }
        
        /*CartApiClient.addLineItem(Order.currentOrder!.id, data: requestData(), success: { order in
            Order.currentOrder = order
            
            let ac = UIAlertController.init(title: "Yoo Hoo!!!", message: "Item was successfully added to your cart!", preferredStyle: .alert)
            let okButton = UIAlertAction.init(title: "OK", style: .default, handler: nil)
            
            ac.addAction(okButton)
            self.present(ac, animated: true, completion: nil)
            
            self.goToCartButton.isHidden = false
            self.addToCartButton.isHidden = true
            
            self.refreshCartItemBadgeCount()
        }, failure: { apiError in
            self.showApiErrorAlert(apiError)
        })*/
    }
    
    func fillInProductDetails() {
        
        print("getproductnamefromdetail")
        print(product?.name)
        print("getproductnamefromdetail")
        
        /*  productDetailsView.configure(for: product!)
        
        if let imageURLs = product?.imageURLs {
            galleryView.delegate = self
            galleryView.imageURLs = imageURLs
            galleryView.setup()
        }
        
        if let properties = product?.properties {
            productPropertiesView.properties = properties
            productPropertiesView.setup()
        }
        
        if product!.hasVariants {
            productVariantsView.product = product!
            productVariantsView.isHidden = false
        } else {
            selectedVariant = product!.masterVariant
        }*/
        
    }
}

extension ProductDetailViewController {
    
    func showAlert(_ title: String, message: String, handler: ((UIAlertAction) -> Void)?) {
        let ac = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default, handler: handler)
        ac.addAction(ok)
        present(ac, animated: true, completion: nil)
    }
    
    func showApiSuccessAlert(_ message: String) {
        showAlert("Yippiee!!!", message: message, handler: { _ in
            self.dismiss(animated: true, completion: nil)
        })
    }
    
    func showApiErrorAlert(_ apiError: ApiError) {
        showAlert("Whooops!!!", message: apiError.errorMessage(), handler: nil)
    }
    
    
}
    


