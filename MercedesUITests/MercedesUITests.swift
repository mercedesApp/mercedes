//
//  MercedesUITests.swift
//  MercedesUITests
//
//  Created by El houssaine El gamouz  on 29/05/2018.
//  Copyright © 2018 majjane. All rights reserved.
//

import XCTest

class MercedesUITests: XCTestCase {
    
    var app: XCUIApplication!
    
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        app = XCUIApplication()
        app.launch()
        
        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testShop() {
        var helloWorld:String?
        XCTAssertNil(helloWorld)
        helloWorld = "hello world"
        XCTAssertNil(helloWorld,"hello world1")
    }
    
}
